import React, { Component } from 'react';

import './style.css';
class Header extends Component {
  render() {
    return (
      <header>
        <div className="logo col-sm-3">logo</div>
        <nav className="col-sm-6">
          <ul>
            <li>item1</li>
            <li>item3</li>
            <li>item2</li>
          </ul>
        </nav>
      </header>
    );
  }
}

export default Header;
