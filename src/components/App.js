import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';

import './main.css'

import Header from './header'
import Footer from './footer'
import Main from './main'

class App extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="wrapper">
            <Header />
            <Main />
            <Footer />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
