import React, { Component } from 'react';

import './style.css';

import listPhones from '../../list.json';
import PhoneDetail from './phoneDetail';
import Bascket from './basket';
import UserProductList from './userProductList';

class Main extends Component {
  constructor() {
    super();

    this.state = {closeModal: false, item: {}, amount: 0,
    total: 0, showProducts: false
    };
    
    if(localStorage.productList && localStorage.productList.length > 0) {      
      this.productList = JSON.parse(localStorage.productList)
    }else{
      this.productList = [];
    }

  }
  showDescription(item) {
   this.setState({
     closeModal: true,
    item: item
   })
  }

  buyProduct(price, item) {
    this.productList.push(item);
    localStorage.productList = JSON.stringify(this.productList);
    this.setState({
      amount: this.state.amount*1+1,
      total: this.state.total*1 + price,
    })
  
  }

  closeModal() {
    this.setState({
      closeModal: false,
    })
  }

  showProduct () {
    this.setState({
      showProducts: true,
    })  
  }

  closeProducts() {
    this.setState({
      showProducts: false,
    }) 
  }

  render() {
    return (
      <main className="clearfix">
      {
        listPhones.map((item) => {
      
          return (
            <div className="col-sm-4 clerfix" key={item.age}>
              <div className="container-list">
                <div className="click-container" onClick={this.showDescription.bind(this, item)}>
                  <div className="title">
                    {item.name}
                  </div>
                  <div className="image-phone">
                    <img src={require(`./${item.imageUrl}`)} />
                  </div>
                </div>  
                <div className="detail">
                  <div className="price">
                    {item.price} <span>лей</span>
                  </div>
                  <div className="bascket" onClick={this.buyProduct.bind(this, item.price, item)}>
                    <i className="fa fa-cart-arrow-down" aria-hidden="true"></i>
                  </div>
                  <div className="button-detail">
                    <button type="button" className="btn btn-success" onClick={this.showDescription.bind(this, item)}>Подробнее</button>
                  </div>
                </div>
              </div>              
            </div>
          )
        })
      }
        <div className={ this.state.closeModal ? "detail-phone-show" : "detail-phone"}>
          <div className="close" onClick={this.closeModal.bind(this)}>
            <i className="fa fa-window-close" aria-hidden="true"></i>
          </div>
          <PhoneDetail  productList={this.productList}  item={this.state.item} count={this.state}/>
        </div>
        <div className="bascket-general" onClick={this.showProduct.bind(this)}>
          <Bascket count={this.state}/>
        </div>
        
        <div className={this.state.showProducts == true ? "show-products" : 'close-products'}>
          <div className="close" onClick={this.closeProducts.bind(this)}>
            <i className="fa fa-window-close" aria-hidden="true"></i>
          </div>
          <UserProductList productList={this.productList}/>
        </div>
        
      </main>  
    );
  }
}

export default Main;
