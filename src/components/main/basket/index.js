import React, { Component } from 'react';

import './style.css';
class Bascket extends Component {

  componentWillMount(){
    if(localStorage.amount > 0) {
      this.props.count.amount =localStorage.amount;
      this.props.count.total = localStorage.total;
    }

  }

  render() {
    
    let {amount, total} =  this.props.count;

    if(amount > 0) {
      localStorage.amount = amount;
      localStorage.total = total;

    }

    let counter = localStorage.amount;
    let countPrice = localStorage.total;
    return (
        <div className="container-basket">
          <i className="fa fa-shopping-basket" aria-hidden="true"></i>
          <div className="details">
            <span className="amount">Количество товаров: {counter}</span><br/>
            <span className="summa">Сумма: {countPrice}</span>
          </div>
        </div>    
    );
  }
}

export default Bascket;
