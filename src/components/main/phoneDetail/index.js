import React, { Component } from 'react';

import './style.css';
import Bascket from '../basket'


class PhoneDetail extends Component {
  constructor() {
    super();
    this.state = { amount: 0,
    total: 0
    };
  }
  buyProduct(price) {
    
    this.props.count.amount = this.props.count.amount*1 + 1;
    this.props.count.total= Number.parseInt(this.props.count.total) + (Number.parseInt(price));
  }
  render() {
    let {item} = this.props;
    let { productList } = this.props;
    return (
        <div className="full-detail col-sm-10 col-xs-offset-1">
          <div className="title">
            {item.name}
          </div>
          <div className="image-phone">
            <img src={item.imageUrl ? require(`../${item.imageUrl}`) : '' } alt="" />
          </div>
          <div className="description">
            {item.snippet}
          </div>
          <div className="detail">
            <div className="price">
              {item.price} <span>лей</span>
            </div>

          </div>
          <div className="bascket-hidden">
          <Bascket count={this.state} />
          </div>
        </div>
    );
  }
}

export default PhoneDetail;
