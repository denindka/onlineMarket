import React, { Component } from 'react';

import './style.css';
import Form from '../form'


class UserProductList extends Component {
  constructor() {
    super();

    this.state = {show: false}
  }

  showForm() {
    this.setState({
      show: true
    })
  }

  closeForm() {
    this.setState({
      show: false
    })
  }

  render() {
    return (
      <div className="product-detail"> 
        {
        this.props.productList.length == 0 ? <div>Добавьте товар </div>:
          this.props.productList.map((list, i)=> {
            return (
              <div className="product" key={i}>
                <span>{list.name}</span><span> Цена: {list.price} лей</span>
              </div>
            )
          })
        }
        <button  
          disabled={!this.props.productList.length}
          type="button"
          className="btn btn-success" 
          onClick={this.showForm.bind(this)}
          >Сделать заказ</button>
        <div className={this.state.show == true ? "form" : 'close-form'} >
          <div className="close" onClick={this.closeForm.bind(this)}>
            <i className="fa fa-window-close" aria-hidden="true"></i>
          </div>
          <Form />
        </div>
      </div>
    );
  }
}

export default UserProductList;
