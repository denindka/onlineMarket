import React, { Component } from 'react';

import './style.css';

class Form extends Component {
  constructor() {
    super();

  }

	submitData() {
		localStorage.clear()
		alert('Спасибо за покупку!!!')
	}

  render() {
    return (
      <div className="container">
						<div className="row">
							<div className="col-md-8 col-md-offset-2">
							  <form action="">
							  	<div className="row">
									  <div className="col-md-10 col-md-offset-1">
								    <div id="legend">
								      <legend className="">Ваши данные</legend>
								    </div>
								    <div className="control-group">
								      <label className="control-label"  htmlFor="username">Имя: </label>
								      <div className="controls">
								        <input type="text" id="username" name="username" placeholder="" className="input-xlarge" />
								      </div>
								    </div>
								 
										<div className="control-group">
								      <label className="control-label"  htmlFor ="phone-number">Номер Телефона: </label>
								      <div className="controls">
								        <input type="number" id="phone-number" name="phone-number" placeholder="" className="input-xlarge" />
								      </div>
								    </div>

								    <div className="control-group">
								      <div className="controls">
								        <button className="btn btn-success button-registration" onClick={this.submitData.bind(this)}>Купить</button>
								      </div>
								    </div>   
							    </div>
							  </div>    
						</form>
					</div>
				</div>
			</div>	
    );
  }
}

export default Form;
